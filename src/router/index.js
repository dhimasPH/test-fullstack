import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        name : 'HelloWorld',
        component: () => import("../components/HelloWorld.vue")
    },
    {
      path: '/optional-challenge',
      name: 'Optional',
      component: () => import("../components/Optional.vue")
    }
    
  ]
  
  const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
  })
  
  export default router
  